#include "../code/Dataset.hpp"

int main(int argc, char **argv) {
	Dataset *dataset = new Dataset();

	dataset->read_books(argv[1]);
	dataset->read_reviews();
	Book* result = dataset->best_book_in_genre();

	print_book(result->get_id(), result->get_title(), result->get_genre_1(),
				result->get_genre_2(), result->get_author(), result->get_pages(), result->get_rate());

  	return 0;
}



