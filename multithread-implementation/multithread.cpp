#include "../code/Dataset.hpp"

struct thread_arg {
    std::string genre;
    int tid;
};

void* best_book_search(void* targs) {
	struct thread_arg *args = (thread_arg*)targs;
	
	Dataset *dataset = new Dataset();

	dataset->read_books_k(args->genre, args->tid);

	dataset->read_reviews();
	Book* result = dataset->best_book_in_genre();
	
	pthread_exit((void*)result);
}

int main(int argc, char** argv) {
	struct thread_arg args;
  	args.genre = argv[1];
	void* result;
	Book* best_book;
	
	pthread_t threads[N_THREADS];
	int return_code;
	for(int tid = 0; tid < N_THREADS; tid++) {
		args.tid = tid;
		return_code = pthread_create(&threads[tid], NULL, best_book_search, (void*)&args);
	}

	for(long tid = 0; tid < N_THREADS; tid++) {
		return_code = pthread_join(threads[tid], &result);
		if(((Book*)result)->get_rate() > best_book->get_rate())
			best_book = (Book*)result;
	}
	
	print_book(best_book->get_id(), best_book->get_title(), best_book->get_genre_1(),
				best_book->get_genre_2(), best_book->get_author(), best_book->get_pages(), best_book->get_rate());

	pthread_exit(NULL);

	return 0;
}