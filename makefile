CC=g++
STD=-std=c++11 -Wall -pedantic

all: serial multithread

serial: serial.o Def.o Utils.o Book.o Dataset.o
	$(CC) $(STD) -g compile/serial.o compile/Def.o compile/Utils.o compile/Book.o compile/Dataset.o -o serial
serial.o: serial-implementation/serial.cpp
	$(CC) $(STD) -c serial-implementation/serial.cpp -o compile/serial.o
Def.o: code/Def.cpp
	$(CC) $(STD) -c code/Def.cpp -o compile/Def.o
Utils.o: code/utils.cpp
	$(CC) $(STD) -c code/utils.cpp -o compile/utils.o
Book.o: code/Book.cpp
	$(CC) $(STD) -c code/Book.cpp -o compile/Book.o
Dataset.o: code/Dataset.cpp
	$(CC) $(STD) -c code/Dataset.cpp -o compile/Dataset.o

multithread: multithread.o Def.o Utils.o Book.o Dataset.o
	$(CC) $(STD) -g compile/multithread.o compile/Def.o compile/Utils.o compile/Book.o compile/Dataset.o -o multithread
multithread.o: multithread-implementation/multithread.cpp
	$(CC) $(STD) -c multithread-implementation/multithread.cpp -o compile/multithread.o

clean:
	rm -rf compile/*.o serial multithread
