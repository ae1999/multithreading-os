#include "Dataset.hpp"

int Dataset::find_book(int id) {
	for(int i = 0; i < books.size(); i++)
		if(id == books[i]->get_id())
			return i;
	return -1;
}

void Dataset::read_books(std::string genre) {
	std::ifstream fin;
	fin.open(BOOK_PATH); 
	std::string temp;
	getline(fin, temp);
	std::vector <std::string> features;
	while(getline(fin, temp)) {
		std::string a = temp;
		std::stringstream s(temp);
		while(getline(s, temp, ','))
			features.push_back(temp);
		if(genre == features[2] || genre == features[3]) {
			Book *new_book = new Book(features[1], features[2],
										features[3], features[5],
										std::stoi(features[0]),
										std::stoi(features[4]),
										std::stof(features[6]));
			books.push_back(new_book);
		}
		features.clear();
	}
	fin.close();
}

void Dataset::read_books_k(std::string genre, int k) {
	std::ifstream fin;
	fin.open(BOOK_PATH); 
	std::string temp;
	getline(fin, temp);
	std::vector <std::string> features;
	int i = 0;
	while(getline(fin, temp)) {
		if(i%N_THREADS == k) {
			std::string a = temp;
			std::stringstream s(temp);
			while(getline(s, temp, ','))
				features.push_back(temp);
			if(genre == features[2] || genre == features[3]) {
				Book *new_book = new Book(features[1], features[2],
											features[3], features[5],
											std::stoi(features[0]),
											std::stoi(features[4]),
											std::stof(features[6]));
				books.push_back(new_book);
			}
		}
		features.clear();
		i++;
	}
	fin.close();
}

void Dataset::read_reviews() {
	std::ifstream fin;
	fin.open(REVIEW_PATH); 
	std::string temp;
	getline(fin, temp);
	std::vector <std::string> features;
	while(getline(fin, temp)) {
		std::stringstream s(temp);
		while(getline(s, temp, ','))
			features.push_back(temp);
		int book = find_book(std::stoi(features[0]));
		if(book != -1)
			books[book]->add_review(std::stoi(features[1]), std::stoi(features[2]));
		features.clear();
	}
	fin.close();
}

Book* Dataset::best_book_in_genre() {
	float max_rate = 0;
	int max_id = -1;
	for(int i = 0; i < books.size(); i++)
		if(books[i]->get_rate() > max_rate)
			max_id = i, max_rate = books[i]->get_rate();
	return books[max_id];
}