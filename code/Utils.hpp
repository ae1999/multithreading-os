#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include "Def.hpp"

void print_book(int id, std::string title, std::string book_genre_1,
				std::string book_genre_2, std::string author_name, int pages, float rating);

#endif
