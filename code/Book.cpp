#include "Book.hpp"

Book::Book(std::string _title,
		std::string _genre_1,
		std::string _genre_2,
		std::string _author,
		int _id, int _pages,
		float _author_rate): 
		title(_title), genre_1(_genre_1),
		genre_2(_genre_2), author(_author), id(_id),
		pages(_pages), author_rate(_author_rate)
		{book_rate = 0; rev_cros_likes_sum = 0; total_likes = 0;}

void Book::add_review(int r, int l) {
	rev_cros_likes_sum += r*l;
	total_likes += l;
	if(!total_likes)
		return;
	book_rate = rev_cros_likes_sum/total_likes;
}