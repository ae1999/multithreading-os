#include "Utils.hpp"

std::string float_format(std::string in) {
	int till = 0; std::string res = "";
	for(int i = 0; i < in.size(); i++)
		if(in[i] == '.')
			till = i;
	for(int i = 0; i < till + 3; i++)
		res += in[i];
	return res;
}

void print_book(int id, std::string title, std::string book_genre_1,
				std::string book_genre_2, std::string author_name, int pages, float rating) {
	std::string res = "id: " + std::to_string(id); endline;
	res += "Title: " + title; endline;
	res += "Genres: " + book_genre_1 + ", " + book_genre_2 +
			" Number of Pages: " + std::to_string(pages) +
			" Author: " + author_name; endline;
	res += "Average Rating: " + float_format(std::to_string(rating));
  std::cout<<std::setprecision(2)<<res<<std::endl;
}