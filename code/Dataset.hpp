#ifndef _DATASET_HPP_
#define _DATASET_HPP_

#include "Book.hpp"

class Dataset {
private:
	std::vector <Book*> books;

public:
	void read_books(std::string genre);
	void read_books_k(std::string genre, int k);
  	void read_reviews();
  	Book* best_book_in_genre();
	int find_book(int id);
};

#endif
