#ifndef _DEF_HPP_
#define _DEF_HPP_

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <fstream>
#include <sys/wait.h>
#include <sys/stat.h>
#include <bits/stdc++.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <algorithm>
#include <limits.h>
#include <pthread.h>

#define BOOK_PATH "/Users/a.e/Documents/uni/s6/os/os projects/3/multithreading-os/datasets/books.csv"
#define REVIEW_PATH "/Users/a.e/Documents/uni/s6/os/os projects/3/multithreading-os/datasets/reviews.csv"
#define DONE std::cout<<"done!"<<std::endl;
#define endline res+="\n";
const int N_THREADS = 8;

#endif