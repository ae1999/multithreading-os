#ifndef _BOOK_HPP_
#define _BOOK_HPP_

#include "Utils.hpp"

class Book {
private:
	std::string title;
	std::string genre_1;
	std::string genre_2;
	std::string author;
	int id;
	int pages;
	float author_rate;
	float book_rate;

	int rev_cros_likes_sum;
	int total_likes;

public:
	Book(std::string _title, std::string _genre_1, std::string _genre_2, std::string _author, int _id, int _pages, float _author_rate);
	void add_review(int r, int l);
	int get_id() {return id;}
	float get_rate() {return author_rate + book_rate;}
	std::string get_author() {return author;}
	std::string get_genre_1() {return genre_1;}
	std::string get_genre_2() {return genre_2;}
	std::string get_title() {return title;}
	int get_pages() {return pages;}
};

#endif
